import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {
        String hello = "hello";
        String world = "world";

        int num = 10;

        System.out.println(hello + " " + world + " " + num);

        StringBuilder builder = new StringBuilder(hello + " " + world);

        int startIndex = hello.length() + 1;
        int endIndex = hello.length() + world.length() + 1;
        builder.replace(startIndex, endIndex, "Java");

        System.out.println(builder);

        double d1 = 1.00001;
        double d2 = 1.0000099999999;

        BigDecimal a = new BigDecimal(d1);
        BigDecimal b = new BigDecimal(d2);

        System.out.println(a.compareTo(b));
    }
}
